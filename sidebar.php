<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package NamNCN
 */

if ( ! is_active_sidebar( $namncn_sidebar = NCN_Sidebar::get_sidebar() ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area col-md-3 col-sm-4 col-xs-12" role="complementary">
	<?php dynamic_sidebar( $namncn_sidebar ); ?>
</aside><!-- #secondary -->
