<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package NamNCN
 */

if ( ! function_exists( 'namncn_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function namncn_posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	$posted_on = sprintf(
		esc_html_x( 'Posted on %s', 'post date', 'namncn' ),
		'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
	);

	$byline = sprintf(
		esc_html_x( 'by %s', 'post author', 'namncn' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);

	echo '<span class="posted-on">' . $posted_on . '</span><span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.

}
endif;

if ( ! function_exists( 'namncn_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function namncn_entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ', ', 'namncn' ) );
		if ( $categories_list && namncn_categorized_blog() ) {
			printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'namncn' ) . '</span>', $categories_list ); // WPCS: XSS OK.
		}

		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', esc_html__( ', ', 'namncn' ) );
		if ( $tags_list ) {
			printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'namncn' ) . '</span>', $tags_list ); // WPCS: XSS OK.
		}
	}

	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link">';
		/* translators: %s: post title */
		comments_popup_link( sprintf( wp_kses( __( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'namncn' ), array( 'span' => array( 'class' => array() ) ) ), get_the_title() ) );
		echo '</span>';
	}

	edit_post_link(
		sprintf(
			/* translators: %s: Name of current post */
			esc_html__( 'Edit %s', 'namncn' ),
			the_title( '<span class="screen-reader-text">"', '"</span>', false )
		),
		'<span class="edit-link">',
		'</span>'
	);
}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function namncn_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'namncn_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'namncn_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so namncn_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so namncn_categorized_blog should return false.
		return false;
	}
}

/**
 * Small function to display site copyright.
 *
 * @param  string $before Before output copyright.
 * @param  string $after  After output copyright.
 * @param  bool   $echo   Echo or return output.
 */
function namncn_site_copyright( $before = '', $after = '', $echo = true ) {
	if ( ! $copyright = namncn_option( 'copyright', null, false ) ) {
		return;
	}

	$theme  = wp_get_theme();
	$search = array(
		'{c}',
		'{year}',
		'{sitename}',
		'{theme}',
		'{author}',
	);

	$replace = array(
		' &copy; ',
		date( 'Y' ),
		get_bloginfo( 'name' ),
		sprintf( esc_html__( '%1$s by %2$s', 'namncn' ), $theme->name, $theme->display( 'Author' ) ),
		$theme->display( 'Author' ),
	);

	$output  = $before;
	$output .= str_replace( $search, $replace, $copyright );
	$output .= $after;

	/**
	 * Fire a filter $output.
	 *
	 * @var string
	 */
	$output = apply_filters( 'namncn_site_copyright', $output );

	if ( ! $echo ) {
		return $output;
	}

	print $output; // WPCS: XSS OK.
}

/**
 * Output a comment in the HTML5 format.
 *
 * @param object $comment Comment to display.
 * @param array  $args    An array of arguments.
 * @param int    $depth   Depth of comment.
 */
function namncn_html5_comment( $comment, $args, $depth ) {
	$tag = ( 'div' === $args['style'] ) ? 'div' : 'li'; ?>

	<<?php echo esc_html( $tag ); ?> id="comment-<?php comment_ID(); ?>" <?php comment_class( 'comment__item' ); ?>>

	<article id="div-comment-<?php comment_ID(); ?>" class="comment-body">
		<?php if ( 0 != $args['avatar_size'] ) : ?>
			<div class="comment__left">
				<div class="comment__thumb"><?php echo get_avatar( $comment, $args['avatar_size'] ); ?></div>
			</div><!-- .awe-comments__left -->
		<?php endif ?>

		<div class="comment__body">
			<div class="comment__action">
				<?php comment_reply_link( array_merge( $args, array(
					'depth'     => $depth,
					'max_depth' => $args['max_depth'],
					'add_below' => 'div-comment',
				) ) ); ?>

				<?php edit_comment_link( esc_html__( 'Edit', 'namncn' ) ); ?>
			</div>

			<div class="comment-metadata awe-comments__meta">
				<span class="comment-author comment__name h6">
					<?php echo get_comment_author_link(); ?>
				</span><!-- .comment-author -->

				<span class="comment__time">
					<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID, $args ) ); ?>">
						<time datetime="<?php comment_time( 'c' ); ?>">
							<?php printf( esc_html__( '%1$s at %2$s', 'namncn' ), get_comment_date( '', $comment ), get_comment_time() ); ?>
						</time>
					</a>
				</span>

				<?php if ( '0' == $comment->comment_approved ) : ?>
					<p class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'namncn' ); ?></p>
				<?php endif; ?>
			</div><!-- .comment-metadata -->

			<div class="comment__content">
				<?php comment_text(); ?>
			</div><!-- .comment-content -->
		</div>

	</article><!-- .comment-body --><?php
	// Note: No close tag is here.
}

/**
 * Flush out the transients used in namncn_categorized_blog.
 */
function namncn_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'namncn_categories' );
}
add_action( 'edit_category', 'namncn_category_transient_flusher' );
add_action( 'save_post',     'namncn_category_transient_flusher' );
