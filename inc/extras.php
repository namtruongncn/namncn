<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package NamNCN
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function namncn_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'namncn_body_classes' );

/**
 * Adds custom classes to the array of layout classes.
 *
 * @param array $classes Classes for the layout element.
 */
function namncn_layout_class( $classes ) {
	$classes = (array) $classes;

	if ( NCN_Sidebar::has_sidebar() ) {
		$classes[] = sprintf( 'sidebar-%s', NCN_Sidebar::get_sidebar_area() );
	} else {
		$classes[] = 'no-sidebar';
	}

	$classes = apply_filters( 'namncn_layout_class', $classes );

	echo 'class="' . esc_attr( join( ' ', $classes ) ) . '"';
}

/**
 * Return blog layout.
 *
 * @param  bool $transform //.
 * @return string
 */
function namncn_blog_layout( $transform = true ) {
	// Get default blog layout.
	$default = namncn_default_blog_layout();

	if ( is_category() || is_tag() || is_tax() ) {
		$term = get_queried_object();
		$layout = namncn_option( 'namncn_blog.layout_category', $default );

		if ( $meta_data = get_term_meta( $term->term_id, 'namncn-blog-layout', true ) ) {
			$layout = $meta_data;
		}
	} elseif ( is_home() ) {
		$layout = namncn_option( 'namncn_blog.layout_home', $default );
	} else {
		$layout = namncn_option( 'namncn_blog.layout_archive', $default );
	}

	return apply_filters( 'namncn_blog_layout', $layout );
}

/**
 * Helper fallback menu.
 */
function fallback() {
	$fallback_menu = '<div class="main-nav"><ul id="fallback-menu" class="menu clearfix"><li><a href="%1$s" rel="home">%2$s</a></li></ul></div>';
	printf( $fallback_menu, esc_url( home_url( '/' ) ), esc_html__( 'Home', 'namncn' ) ); // WPCS: XSS OK.
}

/**
 * Helper fallback mobile menu.
 */
function mobile_fallback() {
	$fallback_menu = '<ul id="fallback-mobile-menu" class="mobile-menu"><li><a href="%1$s" rel="home">%2$s</a></li></ul>';
	printf( $fallback_menu, esc_url( home_url( '/' ) ), esc_html__( 'Home', 'namncn' ) ); // WPCS: XSS OK.
}

/**
 * The excerpt length.
 */
function namncn_excerpt_length() {
	return 30;
}
add_filter( 'excerpt_length', 'namncn_excerpt_length', 999 );

/**
 * The excerpt more.
 *
 * @return sring
 */
function namncn_excerpt_more() {
	return '&hellip; <a href="'. esc_url( get_the_permalink() ) .'" class="more-link">'. esc_html__( 'Read more', 'namncn' ).'</a>';
}
add_filter( 'excerpt_more', 'namncn_excerpt_more' );
