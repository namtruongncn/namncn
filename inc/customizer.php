<?php
/**
 * NamNCN Theme Customizer.
 *
 * @package NamNCN
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function namncn_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
}
add_action( 'customize_register', 'namncn_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function namncn_customize_preview_js() {
	wp_enqueue_script( 'namncn_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'namncn_customize_preview_js' );

final class NCN_Customizer_Manager {

	public static function init() {
		add_action( 'customize_register', array( __CLASS__, 'register' ) );
	}

	public static function register( WP_Customize_Manager $wp_customize ) {
		static::register_topbar( $wp_customize );
		static::register_header( $wp_customize );
		static::register_footer( $wp_customize );

	}

	/**
	 * Register footer setting for the Theme Customizer.
	 *
	 * @param  WP_Customize_Manager $wp_customize Theme Customizer object.
	 */
	public static function register_topbar( WP_Customize_Manager $wp_customize ) {
		$wp_customize->add_section( 'topbar', array(
			'title'    => esc_html__( 'Topbar', 'namncn' ),
			'priority' => 2,
		) );

		$wp_customize->add_setting( 'topbar_left', array(
			'default'           => namncn_default( 'topbar_left' ),
			'transport'         => 'postMessage',
			'sanitize_callback' => array( __CLASS__, 'sanitize_value' ),
		) );

		$wp_customize->add_control( 'topbar_left', array(
			'type'    => 'text',
			'section' => 'topbar',
			'label'   => esc_html__( 'Topbar left', 'namncn' ),
		) );

		$wp_customize->add_setting( 'topbar_center', array(
			'default'           => namncn_default( 'topbar_center' ),
			'transport'         => 'postMessage',
			'sanitize_callback' => array( __CLASS__, 'sanitize_value' ),
		) );

		$wp_customize->add_control( 'topbar_center', array(
			'type'    => 'text',
			'section' => 'topbar',
			'label'   => esc_html__( 'Topbar Center', 'namncn' ),
		) );

		$wp_customize->add_setting( 'facebook', array(
			'default'           => namncn_default( 'facebook' ),
			'transport'         => 'postMessage',
			'sanitize_callback' => array( __CLASS__, 'sanitize_value' ),
		) );

		$wp_customize->add_control( 'facebook', array(
			'type'     => 'text',
			'section'  => 'topbar',
			'label'    => esc_html__( 'Facebook', 'namncn' ),
		) );

		$wp_customize->add_setting( 'twitter', array(
			'default'           => namncn_default( 'twitter' ),
			'transport'         => 'postMessage',
			'sanitize_callback' => array( __CLASS__, 'sanitize_value' ),
		) );

		$wp_customize->add_control( 'twitter', array(
			'type'    => 'text',
			'section' => 'topbar',
			'label'   => esc_html__( 'Twitter', 'namncn' ),
		) );

		$wp_customize->add_setting( 'google', array(
			'default'           => namncn_default( 'google' ),
			'transport'         => 'postMessage',
			'sanitize_callback' => array( __CLASS__, 'sanitize_value' ),
		) );

		$wp_customize->add_control( 'google', array(
			'type'    => 'text',
			'section' => 'topbar',
			'label'   => esc_html__( 'Google', 'namncn' ),
		) );

		// Support selective_refresh.
		$wp_customize->selective_refresh->add_partial( 'topbar', array(
			'selector'            => '.topbar',
			'settings'            => array( 'topbar_left', 'topbar_center', 'facebook', 'twitter', 'google' ),
			'container_inclusive' => true,
			'render_callback'     => function() {
				get_template_part( 'template-parts/header-topbar' );
			},
		) );
	}

	/**
	 * Register header setting for the Theme Customizer.
	 *
	 * @param  WP_Customize_Manager $wp_customize Theme Customizer object.
	 */
	public static function register_header( WP_Customize_Manager $wp_customize ) {
		$wp_customize->add_section( 'header', array(
			'title'    => esc_html__( 'Header', 'namncn' ),
			'priority' => 3,
		) );

		$wp_customize->add_setting( 'intro', array(
			'default'           => namncn_default( 'intro' ),
			'transport'         => 'postMessage',
			'sanitize_callback' => array( __CLASS__, 'sanitize_value' ),
		) );

		$wp_customize->add_control( 'intro', array(
			'type'    => 'text',
			'section' => 'header',
			'label'   => esc_html__( 'Intro', 'namncn' ),
		) );

		$wp_customize->add_setting( 'subintro', array(
			'default'           => namncn_default( 'subintro' ),
			'transport'         => 'postMessage',
			'sanitize_callback' => array( __CLASS__, 'sanitize_value' ),
		) );

		$wp_customize->add_control( 'subintro', array(
			'type'    => 'text',
			'section' => 'header',
			'label'   => esc_html__( 'SubIntro', 'namncn' ),
		) );

		$wp_customize->add_setting( 'contact', array(
			'default'           => namncn_default( 'contact' ),
			'transport'         => 'postMessage',
			'sanitize_callback' => array( __CLASS__, 'sanitize_value' ),
		) );

		$wp_customize->add_control( 'contact', array(
			'type'     => 'text',
			'section'  => 'header',
			'label'    => esc_html__( 'Contact', 'namncn' ),
		) );

		// Support selective_refresh.
		$wp_customize->selective_refresh->add_partial( 'header', array(
			'selector'            => '.site-header',
			'settings'            => array( 'intro', 'subintro', 'contact' ),
			'container_inclusive' => false,
			// 'render_callback'     => function() {
			// },
		) );
	}

	/**
	 * Register footer setting for the Theme Customizer.
	 *
	 * @param  WP_Customize_Manager $wp_customize Theme Customizer object.
	 */
	public static function register_footer( WP_Customize_Manager $wp_customize ) {
		$wp_customize->add_section( 'footer', array(
			'title'    => esc_html__( 'Footer', 'namncn' ),
			'priority' => 25,
		) );

		// Footer copyright setting.
		$wp_customize->add_setting( 'copyright', array(
			'default'   => namncn_default( 'copyright' ),
			'transport' => 'postMessage',
			'sanitize_callback' => array( __CLASS__, 'sanitize_value' ),
		) );

		$wp_customize->add_control( 'copyright', array(
			'type'    => 'textarea',
			'section' => 'footer',
			'label'   => esc_html__( 'Copyright', 'namncn' ),
			'description' => esc_html__( 'Tips: Use {c}, {year}, {sitename}, {theme}, {author} to generate dynamic value.', 'namncn' ),
		) );

		$wp_customize->selective_refresh->add_partial( 'copyright', array(
			'selector'            => '.site-copyright',
			'container_inclusive' => false,
			'render_callback'     => function() {
				namncn_site_copyright();
			},
		) );
	}

	/**
	 * Sanitize raw value.
	 *
	 * @param  mixed $value Raw string value.
	 * @return string
	 */
	public static function sanitize_value( $value ) {
		return $value;
	}

}
NCN_Customizer_Manager::init();
