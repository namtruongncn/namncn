<?php
/**
 * This file represents an example of the code that themes would use to register
 * the required plugins.
 *
 * @see https://github.com/TGMPA/TGM-Plugin-Activation/blob/develop/example.php
 *
 * @package NamNCN
 */

/**
 * Register the required plugins for this theme.
 */
function namncn_register_required_plugins() {
	/*
	 * Array of plugin arrays. Required keys are name and slug.
	 */
	$plugins = array(
		array(
			'name' => esc_html__( 'NamNCN Required', 'namncn' ),
			'slug' => 'namncn-required',
			'source' => get_template_directory_uri() . '/inc/plugins/namncn-required.zip',
			'version' => '1.0.0',
			'required' => true,
		),
		array(
			'name' => esc_html__( 'Contact Form 7', 'namncn' ),
			'slug' => 'contact-form-7',
			'version' => '4.4.2',
			'required' => true,
		),
		array(
			'name' => esc_html__( 'Contact Form 7 - Dynamic Text Extension', 'namncn' ),
			'slug' => 'contact-form-7-dynamic-text-extension',
			'version' => '2.0.1',
			'required' => true,
		),
		array(
			'name' => esc_html__( 'Easy FancyBox', 'namncn' ),
			'slug' => 'easy-fancybox',
			'version' => '1.5.7',
			'required' => true,
		),
		array(
			'name' => esc_html__( 'Yoast SEO', 'namncn' ),
			'slug' => 'wordpress-seo',
			'version' => '3.3.4',
			'required' => true,
		),
		array(
			'name' => esc_html__( 'WPBakery Visual Composer', 'namncn' ),
			'slug' => 'js_composer',
			'source' => get_template_directory_uri() . '/inc/plugins/js_composer.zip',
			'version' => '4.12',
			'required' => true,
		),
	);

	/*
	 * Array of configuration settings. Amend each line as needed.
	 */
	$config = array(
		'id' => 'namncn',
		'is_automatic' => true,
		'strings' => array( 'nag_type' => 'notice-warning' ),
	);

	tgmpa( $plugins, $config );
}
add_action( 'tgmpa_register', 'namncn_register_required_plugins' );
