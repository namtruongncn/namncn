<?php
/**
 * The template for displaying all product posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package NamNCN
 */

get_header(); ?>

	<div id="primary" class="content-area col-md-7 col-sm-8 col-xs-12 col-md-offset-1">
		<main id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', 'product' );

			the_post_navigation( array(
				'prev_text' => '<i class="fa fa-angle-double-left"></i> ' . esc_html__( 'Previous product', 'namncn' ),
				'next_text' => esc_html__( 'Next product', 'namncn' ) . ' <i class="fa fa-angle-double-right"></i>',
			));

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
