<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package NamNCN
 */

global $postdata;

/**
 * The content of the current post.
 *
 * @var string
 */
$the_content = get_the_content( sprintf(
	wp_kses( __( 'Continue reading %s <span class="meta-nav">&hellip;</span>', 'namncn' ), array( 'span' => array( 'class' => array() ) ) ),
	the_title( '<span class="screen-reader-text">"', '"</span>', false )
) );

/**
 * Parse post content by post format.
 *
 * @var string
 */
$postdata = NCN_Post_Format::parse_post_content( $the_content );

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php get_template_part( 'template-parts/feature-image' ); ?>
		<?php the_title( '<h1 class="entry-title">', '</h1>' );

		if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php namncn_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php
		endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'namncn' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php namncn_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
