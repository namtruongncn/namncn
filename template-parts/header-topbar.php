<div class="topbar">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<p class="intro"><?php namncn_option( 'topbar_left' ); ?></p>
			</div><!-- .col-md-6 -->
			<div class="col-md-3">
				<div class="topbar-contact">
					<a href="tel:<?php namncn_option( 'topbar_center' ); ?>" class="topbar-phone">
						<i class="fa fa-phone"></i>
						<span class="topbar-span"><?php namncn_option( 'topbar_center' ); ?></span>
					</a>
				</div><!-- .topbar-contact -->
			</div><!-- .col-md-2 -->
			<div class="col-md-2">
				<div class="topbar-social">
					<?php if ( namncn_option( 'facebook', null, false ) ) : ?>
					<a href="<?php namncn_option( 'facebook' ); ?>" class="social-item" title="">
						<i class="fa fa-facebook "></i>
					</a>
					<?php endif; ?>
					<?php if ( namncn_option( 'twitter', null, false ) ) : ?>
					<a href="http://twitter.com/<?php namncn_option( 'twitter' ); ?>" class="social-item" title="">
						<i class="fa fa-twitter "></i>
					</a>
					<?php endif; ?>
					<?php if ( namncn_option( 'google', null, false ) ) : ?>
					<a href="<?php namncn_option( 'google' ); ?>" class="social-item" title="">
						<i class="fa fa-google-plus "></i>
					</a>
					<?php endif; ?>
			  </div><!-- .topbar-social -->
			</div><!-- .col-md-2 -->
			<div class="col-md-2">
				<div class="topbar-login">
					<a href="<?php echo admin_url( '', 'admin' ); ?>">
					<i class="fa fa-user"></i>
					<span><?php esc_html_e( 'Log in/Sign up', 'namncn' ); ?></span>
					</a>
				</div>
			</div><!-- .col-md-2 -->
		</div>
	</div>
</div>
