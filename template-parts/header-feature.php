<?php if ( is_home() || is_front_page() ) : ?>
<div class="container">
	<div class="row">
		<div class="col-md-8">

			<h2 class="banner__title h1"><?php namncn_option( 'intro' ); ?></h2>
			<h3 class="banner__subtitle"><?php namncn_option( 'subintro' ); ?></h3>
		</div><!-- .col-md-8 -->
		<div class="col-md-4">
			<div class="header-contact">
				<?php echo do_shortcode( namncn_option( 'contact', null, false ) ); ?>
			</div>
		</div>
	</div><!-- .row -->
</div><!-- .container -->
<?php endif; ?>
