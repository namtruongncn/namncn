<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package NamNCN
 */

$product_info = get_post_meta( get_the_ID(), 'ncn_product', true );

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<div class="single-product_thumb">
			<?php the_post_thumbnail(); ?>
			<figure class="item-photo__hover">
				<?php if ( $product_info['real_price'] ) : ?>
				<span class="properties__param"><?php printf( '%1$s: - %3$s %2$s', esc_html__( 'Real Price', 'namncn' ), esc_html( $product_info['real_price'] ), esc_html( $product_info['currency'] ) ); ?></span>
				<?php endif; ?>
				<?php if ( $product_info['type'] ) : ?>
				<span class="properties__param"><?php printf( '%1$s: - %2$s', esc_html__( 'Website Type', 'namncn' ), esc_html( $product_info['type'] ) ); ?></span>
				<?php endif; ?>
				<?php if ( $product_info['customize'] ) : ?>
				<span class="properties__param"><?php printf( '%1$s: - %2$s', esc_html__( 'Customize Panel', 'namncn' ), esc_html( $product_info['customize'] ) ); ?></span>
				<?php endif; ?>
				<?php if ( $product_info['change'] ) : ?>
				<span class="properties__param"><?php printf( '%1$s: - %2$s', esc_html__( 'Changed on request', 'namncn' ), esc_html( $product_info['change'] ) ); ?></span>
				<?php endif; ?>
				<?php if ( $product_info['shortdesc'] ) : ?>
				<span class="properties__shortdesc"><?php echo esc_html( $product_info['shortdesc'] ); ?></span>
				<?php endif; ?>
			</figure>
		</div><!-- .product_thumb -->

		<div class="product_seedetails">
			<a href="<?php echo esc_html( $product_info['link'] ); ?>" class="product__link" target="_blank"><?php esc_html_e( 'View Demo', 'namncn' ); ?></a>
			<a href="#contact_form_pop" class="product__send fancybox"><?php esc_html_e( 'Contact', 'namncn' ); ?><i class="fa fa-envelope-o"></i></a>
			<div style="display:none" class="fancybox-hidden">
				<div id="contact_form_pop">
					<?php echo do_shortcode('[contact-form-7 id="1929" title="Contact"]') ?>
				</div>
			</div>
		</div><!-- .product_seedetails -->
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'namncn' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php namncn_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
