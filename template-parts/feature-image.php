<?php
/**
 * Template part for feature image post.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package NamNCN
 */

global $postdata;

/**
 * //
 *
 * @var array
 */
$parser_data = wp_parse_args( $postdata['data'], array(
	'output' => '',
	'shortcode' => '',
) );

/**
 * Display feature thumbnail
 */
if ( empty( $postdata['data']['shortcode'] ) ) :

	if ( has_post_thumbnail() ) : ?>
		<div class="post__media">
			<a href="<?php echo esc_url( get_permalink() ); ?>" class="post__thumb">
				<?php the_post_thumbnail( 'large-horizontal' ); ?>
			</a>
		</div>
	<?php elseif ( is_single() ) : ?>
		<div class="post__media post__media--nothumb">
			<a href="#" class="post__thumb">
				<span class="screen-reader-text"><?php echo esc_html__( 'No thumbnail', 'thepearl' ); ?></span>
			</a>
		</div>
	<?php endif;

	return;
endif;

/**
 * Display post format feature
 */
switch ( $postdata['post_format'] ) {
	case 'video':
		printf( '<div class="post__media embed-responsive embed-responsive-16by9">%s</div>', $parser_data['output'] ); // WPCS: XSS OK.
		break;

	case 'audio':
		printf( '<div class="post__media embed-responsive responsive-audio">%s</div>', $parser_data['output'] ); // WPCS: XSS OK.
		break;

	case 'gallery': ?>
		<div class="post__media">
			<div class="post__media--slick">
				<?php foreach ( $parser_data['ids'] as $id ) : ?>
					<div class="item-img"><?php echo wp_get_attachment_image( $id, 'large-horizontal' ); ?></div>
				<?php endforeach; ?>
			</div>
		</div><?php
		break;
}
