<?php
/**
 * Template Name: Page Builder
 * Description: Template for page builder.
 *
 * @package NamNCN
 */

get_header(); ?>
	
	<div id="primary" class="content-area col-md-12">
		<main id="main" class="site-main" role="main">
			<?php while ( have_posts() ) : the_post(); ?>

				<div id="page-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php the_content(); ?>
				</div><!-- #post-## -->

			<?php endwhile; // End of the loop. ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
