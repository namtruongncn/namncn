<?php
/**
 * NamNCN functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package NamNCN
 */

if ( ! function_exists( 'namncn_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function namncn_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on NamNCN, use a find and replace
	 * to change 'namncn' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'namncn', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'horizontal-small', 200, 57, true );
	add_image_size( 'thumbnail-square', 400, 400, true );
	add_image_size( 'thumbnail-horizontal', 380, 238, true );
	add_image_size( 'large-horizontal', 960, 400, true );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'namncn' ),
		'mobile'  => esc_html__( 'Mobile', 'namncn' ),
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'gallery',
		'quote',
		'image',
		'video',
		'audio',
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'namncn_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'namncn_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function namncn_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'namncn_content_width', 640 );
}
add_action( 'after_setup_theme', 'namncn_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function namncn_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'namncn' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'namncn' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebars( 3, array(
		'name'          => esc_html__( 'Footer %d', 'namncn' ),
		'id'            => 'footer',
		'description'   => esc_html__( 'Add widgets here.', 'namncn' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'namncn_widgets_init' );

/**
 * Register Fonts
 */
function namncn_google_fonts_url() {
	$font_url = '';

	/*
	Translators: If there are characters in your language that are not supported
	by chosen font(s), translate this to 'off'. Do not translate into your own language.
	*/
	if ( 'off' !== _x( 'on', 'Google font: on or off', 'namncn' ) ) {
		$font_url = add_query_arg( 'family', urlencode( 'Montserrat:400,700|Source Sans Pro:400,400i,600,600i,700&subset=latin,latin-ext' ), '//fonts.googleapis.com/css' );
	}
	return $font_url;
}

/**
 * Enqueue scripts and styles.
 */
function namncn_scripts() {

	// Register CSS.
	wp_enqueue_style( 'namncn-google-fonts', namncn_google_fonts_url(), array(), null );

	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.3.6' );
	
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '4.6.3' );

	wp_enqueue_style( 'slick', get_template_directory_uri() . '/css/slick.css', array(), '1.6.0' );

	wp_enqueue_style( 'namncn-main', get_template_directory_uri() . '/css/main.css', array(), '1.0.0' );

	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '3.3.6', true );

	wp_enqueue_script( 'slick-slider', get_template_directory_uri() . '/js/slick.min.js', array( 'jquery' ), '1.6.0', true );

	wp_enqueue_script( 'namncn-main', get_template_directory_uri() . '/js/main.js', array( 'jquery' ), '1.0.0', true );

	wp_enqueue_script( 'namncn-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '1.0.0', true );

	wp_enqueue_script( 'namncn-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '1.0.0', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'namncn_scripts' );

/**
 * Get default option for NamNCN.
 *
 * @param  string $name Option key name to get.
 * @return mixin
 */
function namncn_default( $name ) {
	static $defaults;

	if ( ! $defaults ) {
		$defaults = apply_filters( 'namncn_defaults', array(
			// Topbar left.
			'topbar_left'            => esc_html__( 'Hi! my name is Nam NCN, i\'m web developer. Make your website here!', 'namncn' ),
			// Topbar center.
			'topbar_center'          => '+84-1639-482-057',
			// Facebook link
			'facebook'               => 'https://www.facebook.com/nam.truong.ncn/',
			// Twitter Username
			'twitter'                => 'namncn',
			// Google plus Link.
			'google'                 => 'http://plus.google.com/+namncn',
			// Intro.
			'intro'                  => esc_html__( 'The best place to find your website', 'namncn' ),
			// Subintro.
			'subintro'               => esc_html__( 'Nam NCN provides high quality sites, attentive customer support, guaranteed to bring you the best products.', 'namncn' ),
			// Intro.
			'contact'                => '[contact-form-7 id="1716" title="Contact form 1"]',
			// Footer copyright.
			'copyright'              => esc_html__( 'Proudly powered by WordPress', 'namncn' ),
		) );
	}

	return isset( $defaults[ $name ] ) ? $defaults[ $name ] : null;
}

/**
 * Get option for NamNCN.
 *
 * @param  string $name    Customize option key-name.
 * @param  mixed  $default Default value return.
 * @param  bool $echo echo or return.
 * @return mixed|null
 */
function namncn_option( $name, $default = null, $echo = true ) {
	$name = sanitize_key( $name );

	if ( is_null( $default ) ) {
		$default = namncn_default( $name );
	}

	$option = get_theme_mod( $name, $default );

	/**
	 * Apply filter to custom option value.
	 *
	 * @param string $option Option value.
	 *
	 * @var mixed
	 */
	$option = apply_filters( 'namncn_option_' . $name, $option );

	$option = apply_filters( 'namncn_option', $option, $name );

	if ( ! $echo ) {
		return $option;
	}

	print $option; // WPCS: XSS OK.
}

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load sidebar feature class.
 */
require get_template_directory() . '/inc/sidebar.php';

/**
 * Post format support.
 */
require get_template_directory() . '/inc/post-format.php';

/**
 * Include the TGM_Plugin_Activation class.
 */
require get_template_directory() . '/inc/class-tgm-plugin-activation.php';

/**
 * Required Plugins.
 */
require get_template_directory() . '/inc/tgm-register.php';

