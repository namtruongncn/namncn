<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package NamNCN
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'namncn' ); ?></a>

	<header id="masthead" class="site-header" role="banner" style="background: url(<?php header_image(); ?>);background-attachment: fixed;">

		<?php wp_nav_menu( array( 'theme_location' => 'mobile', 'menu_id' => 'mobile-menu', 'menu_class' => 'mobile-menu close', 'container' => '', 'fallback_cb' => 'mobile_fallback' ) ); ?>

		<?php get_template_part( 'template-parts/header-topbar' ); ?>

		<nav id="site-navigation" class="main-navigation" role="navigation">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<div class="site-branding">
							<?php
							if ( is_front_page() && is_home() ) : ?>
								<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
							<?php else : ?>
								<p class="site-title h1"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
							<?php
							endif;

							$description = get_bloginfo( 'description', 'display' );
							if ( $description || is_customize_preview() ) : ?>
								<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
							<?php
							endif; ?>
						</div><!-- .site-branding -->
						<div class="mobile-menu-toggle open">
							<span class="screen-reader-text">Toggle navigation</span>
							<span class="icon-bar icb-1"></span>
							<span class="icon-bar icb-2"></span>
							<span class="icon-bar icb-3"></span>
						</div>
					</div>
					<div class="col-md-9">
						<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu', 'menu_class' => 'menu clearfix', 'container_class' => 'main-nav', 'fallback_cb' => 'fallback' ) ); ?>
					</div>
				</div><!-- .row -->
			</div><!-- .container -->
		</nav><!-- #site-navigation -->

		<?php get_template_part( 'template-parts/header-feature' ); ?>

	</header><!-- #masthead -->

	<div id="content" <?php namncn_layout_class( 'site-content' ); ?>>
		<div class="container">
			<div class="row">
