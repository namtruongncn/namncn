(function($) {
  'use strict';

  $('.review__item').slick({
  	dots: true,
  	autoplay: true,
  	arrows: false,
  });

  $('.partner__slider').slick({
  	dots: false,
  	autoplay: true,
  	infinite: true,
  	slidesToShow: 5,
  	prevArrow: '<button type="button" class="slick-prev"></button>',
  	nextArrow: '<button type="button" class="slick-next"></button>',
  	responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 1,
	        infinite: true,
	        dots: false,
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 1,
	        dots: false,
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
	        dots: false,
	      }
	    }
	]
  });

  $('.post__media--slick').slick({
  	dots: false,
  	autoplay: true,
  	arrows: true,
  	prevArrow: '<button type="button" class="slick-prev"></button>',
  	nextArrow: '<button type="button" class="slick-next"></button>',
  });

  $('.mobile-menu').on( 'click', '.menu-item-has-children i', function(){
  	$(this).parent('.menu-item-has-children').toggleClass('active');
  });

  $('.mobile-menu .menu-item-has-children').prepend('<i class="fa fa-chevron-down"></i>');

  $('.main-navigation').on( 'click', '.mobile-menu-toggle', function(){
  	$('.mobile-menu').toggleClass('active').toggleClass('close');
  	$('.mobile-menu-toggle').toggleClass('close').toggleClass('open');
  });

})(jQuery);
