<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package NamNCN
 */

?>
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">

		<div class="container">
			<div class="row">

				<?php if ( is_active_sidebar( 'footer' ) ) : ?>
				<div class="col-md-5">
					<?php dynamic_sidebar( 'footer' ); ?>
				</div>
				<?php endif; ?>

				<?php if ( is_active_sidebar( 'footer-2' ) ) : ?>
				<div class="col-md-4">
					<?php dynamic_sidebar( 'footer-2' ); ?>
				</div>
				<?php endif; ?>

				<?php if ( is_active_sidebar( 'footer-3' ) ) : ?>
				<div class="col-md-3">
					<?php dynamic_sidebar( 'footer-3' ); ?>
				</div>
				<?php endif; ?>

			</div><!-- .row -->
		</div><!-- .container -->

		<?php namncn_site_copyright( '<div class="site-copyright"><p>', '</p></div>' ); ?>

	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
