<?php
/**
 * Template for displaying search forms.
 *
 * @package NamNCN
 */

?>

<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'namncn' ); ?>" value="<?php echo get_search_query(); ?>" name="s">

	<button type="submit" class="search-submit btn btn-primary">
		<span class="screen-reader-text"><?php echo esc_html_x( 'Search', 'submit button', 'namncn' ); ?></span>
		<i class="fa fa-search" aria-hidden="true"></i>
	</button>
</form>
